import pytest
import requests


# Test endpoint for password hashing
def test_hash_password_success():
    url = "http://localhost:8000/users/testhash/samplepassword"
    response = requests.get(url)
    assert response.status_code == 200


def test_hash_password_empty_string():
    url = "http://localhost:8000/users/testhash/"
    response = requests.get(url)
    assert response.status_code == 404  # Empty string might not be a valid route


# Testing user creation endpoint
def test_create_user_success():
    url = "http://localhost:8000/users/createUser"
    user_data = {
        "description": "",
        "direction": "",
        "email": "ejemplo@ejemplo.com",
        "name": "",
        "password": "12345678",
        "photo_id": 0,
        "surnames": "",
        "username": "nuevo1",
    }
    response = requests.post(url, json=user_data)
    assert response.status_code == 200
    assert "user_id" in response.json()


def test_create_user_failure():
    url = "http://localhost:8000/users/createUser"
    user_data = {
        "description": "",
        "direction": "",
        "email": "ejemplomalo",
        "name": "",
        "password": "",
        "photo_id": 0,
        "surnames": "",
        "username": "nuevo1",
    }  # Invalid data
    response = requests.post(url, json=user_data)
    assert response.status_code == 400


# Testing identity check endpoint
def test_check_identity_valid():
    url = "http://localhost:8000/users/identity/nuevo1/12345678"
    response = requests.get(url)
    assert response.status_code == 200
    assert "user_id" in response.json() and "email" in response.json()


def test_check_identity_invalid_password():
    url = "http://localhost:8000/users/identity/nuevo1/12345678964478554"
    response = requests.get(url)
    assert response.status_code == 404
    assert response.json() == {"detail": "Invalid Password"}


def test_check_identity_user_not_found():
    url = "http://localhost:8000/users/identity/nonexistentuser/password"
    response = requests.get(url)
    assert response.status_code == 403
    assert response.json() == {"detail": "User not found"}


# Testing user information retrieval endpoint
def test_user_info_success():
    url = "http://localhost:8000/users/userInfo/1"
    response = requests.get(url)
    assert response.status_code == 200
    assert "user_info" in response.json()


def test_user_info_not_found():
    url = "http://localhost:8000/users/userInfo/999"  # Assuming 999 is an invalid ID
    response = requests.get(url)
    assert response.status_code == 403
    assert response.json() == {"detail": "User not found"}
