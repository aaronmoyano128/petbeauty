import pytest
import requests
import json


def test_subir_publicacion():
    url = "http://localhost:8000/publication/subirPublicacion"
    data = {
        "user_id": 5,
        "pet_id": 2,
        "photo_id": 2,
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 200


def test_subir_publicacion_error1():
    url = "http://localhost:8000/publication/subirPublicacion"
    data = {
        "user_id": 124,
        "pet_id": 2,
        "photo_id": 2,
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_subir_publicacion_error2():
    url = "http://localhost:8000/publication/subirPublicacion"
    data = {
        "user_id": 5,
        "pet_id": 152,
        "photo_id": 2,
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_subir_publicacion_error3():
    url = "http://localhost:8000/publication/subirPublicacion"
    data = {
        "user_id": 5,
        "pet_id": 2,
        "photo_id": 135,
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_subir_publicacion_error3():
    url = "http://localhost:8000/publication/subirPublicacion"
    data = {
        "user_id": 5,
        "pet_id": 2,
        "photo_id": 2,
        "description": 0,
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_subir_publicacion_error3():
    url = "http://localhost:8000/publication/subirPublicacion"
    data = {
        "user_id": 5,
        "pet_id": 2,
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 422


# Test cases for obtaining all publications
def test_obtener_publicaciones():
    url = "http://localhost:8000/publication/get/"
    response = requests.get(url)
    assert response.status_code == 200
    assert (
        type(response.json()) is list
    )  # Assuming the API returns a list of publications


# Test for adding a rating to a publication
def test_add_rating():
    url = "http://localhost:8000/publication/anadirValoracion/5/3?is_like=true"
    response = requests.put(url)
    assert response.status_code == 200
    assert response.json() == {"modified": True}


# Test for adding a rating with invalid user_id and publication_id
def test_add_rating_invalid_ids():
    url = "http://localhost:8000/publication/anadirValoracion/9999595195151919/99995465181519444?is_like=true"
    response = requests.put(url)
    assert response.status_code == 404


# Test getting the most liked publications by a valid user
def test_get_most_liked_publications():
    url = "http://localhost:8000/publication/mostLiked/5"
    response = requests.get(url)
    assert response.status_code == 200
    assert type(response.json()) is list  # Assuming a list of publications is returned


# Test getting the most liked publications with invalid user_id
def test_get_most_liked_publications_invalid_user():
    url = "http://localhost:8000/publication/mostLiked/9999881816516515189"  # Non-existent user_id
    response = requests.get(url)
    assert response.status_code == 404


# Test retrieving all ratings in the system
def test_obtener_ratings():
    url = "http://localhost:8000/publication/getAllRatings/"
    response = requests.get(url)
    assert response.status_code == 200
    assert type(response.json()) is list  # Assuming the API returns a list of ratings
