# probamos el endpoint /pets/listPets/{userId}

import pytest
import requests
import json


# usuario existente con mascotas
def test_list_pets():
    url = "http://localhost:8000/pets/listPets/2"
    response = requests.get(url)
    assert response.status_code == 200
    assert response.json() == [
        {
            "pet_id": 2,
            "name": "Pitufo",
            "description": "Este es mi perro",
            "species": "Perro",
            "photo_id": 12,
        },
        {
            "pet_id": 9,
            "name": "Trotuman",
            "description": "La totuga mas extrema",
            "species": "Tortuga",
            "photo_id": 15,
        },
    ]


# usuario no existente
def test_list_pets_no_existente():
    url = "http://localhost:8000/pets/listPets/100"
    response = requests.get(url)
    assert response.status_code == 404
    assert response.json() == {"detail": "User not found"}
