# crear un test para el endpoint pets/createPet, depues de utilizar el endpoint, se deberá eliminar el registro creado durante el test, es decir, se debera conectar a la base de datos buscarlo y eliminarlo directamente desde la base de datos.

import pytest
import requests
import json


def test_create_pet():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 5,
        "pet_name": "string",
        "photo_id": 2,
        "species": "Perro",
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 200


# Eliminar el registro creado en el test anterior
def test_delete_pet():
    url = "http://localhost:8000/pets/deletePet/string"
    response = requests.delete(url)
    assert response.status_code == 200
    assert response.json() == {"detail": "Pet deleted"}


# body incorrecto
def test_create_pet_body_incorrecto1():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 51,
        "pet_name": "string",
        "photo_id": 2,
        "species": "Perro",
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_create_pet_body_incorrecto2():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 5,
        "pet_name": "",
        "photo_id": 2,
        "species": "Perro",
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_create_pet_body_incorrecto3():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 5,
        "pet_name": "string",
        "photo_id": 122,
        "species": "Perro",
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_create_pet_body_incorrecto4():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 5,
        "pet_name": "string",
        "photo_id": 2,
        "species": "Pero",
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404


def test_create_pet_body_incorrecto5():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 5,
        "pet_name": "string",
        "photo_id": 2,
        "species": "Perro",
        "description": "",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 404

def test_create_pet_body_incorrecto6():
    url = "http://localhost:8000/pets/createPet"
    data = {
        "user_id": 5,
        "pet_name": "string",
        "species": "Perro",
        "description": "string",
    }
    response = requests.post(url, json=data)
    assert response.status_code == 422
