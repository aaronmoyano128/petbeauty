import unittest
from test_init import InitTest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import time
import os


class TestMascota(InitTest):
    botonMascotas = (By.CSS_SELECTOR, "html > div > div > div:nth-child(4) > svg")
    botonCrearMascota = (
        By.CSS_SELECTOR,
        "html > div > div.container > div:nth-child(6) > div.boton2 > button",
    )
    botonSubirFoto = (By.CSS_SELECTOR, "#fileInput")
    mascotaNueva = (
        By.XPATH,
        "/html/body/app-root/app-mis-mascotas/html/div/div[2]/div[5]/div[2]",
    )

    def test_crear_mascota(self):
        self.login()
        self.waitFor(self.botonMascotas)
        self.driver.find_element(*self.botonMascotas).click()
        self.waitFor(self.botonCrearMascota)
        self.driver.find_element(*self.botonCrearMascota).click()
        self.waitFor(self.botonSubirFoto)
        foto = self.driver.find_element(*self.botonSubirFoto)
        abs_path = os.path.abspath("test\integration\images\conejo.png")
        foto.send_keys(abs_path)
        self.waitFor(self.botonSubirFoto)
        self.driver.find_element(By.ID, "nombre").send_keys("Telma")
        raza = Select(self.driver.find_element(By.ID, "raza"))
        raza.select_by_value("Conejo")
        self.driver.find_element(By.ID, "descripcion").send_keys(
            "Lo más mono que he visto nunca"
        )
        self.driver.find_element(
            By.CSS_SELECTOR,
            "html > div > div.container > div.mascota > div.boton2 > button",
        ).click()
        self.waitFor(self.mascotaNueva)
        nombreMascotaCreada = self.driver.find_element(*self.mascotaNueva).text

        assert "Telma" in nombreMascotaCreada
        assert "Conejo" in nombreMascotaCreada
        assert "Lo más mono que he visto nunca" in nombreMascotaCreada

    def test_crear_mascota_error1(self):
        self.login()
        self.waitFor(self.botonMascotas)

        self.driver.find_element(*self.botonMascotas).click()
        sleep(5)
        self.waitFor(self.botonCrearMascota)
        self.driver.find_element(*self.botonCrearMascota).click()
        self.waitFor(self.botonSubirFoto)
        foto = self.driver.find_element(*self.botonSubirFoto)
        abs_path = os.path.abspath("test\integration\images\conejo.png")
        foto.send_keys(abs_path)
        self.waitFor(self.botonSubirFoto)
        self.driver.find_element(By.ID, "nombre").send_keys("Telma")
        self.driver.find_element(By.ID, "descripcion").send_keys(
            "Lo más mono que he visto nunca"
        )
        self.driver.find_element(
            By.CSS_SELECTOR,
            "html > div > div.container > div.mascota > div.boton2 > button",
        ).click()
        error = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-anadir-mascota/html/div/div[2]/div[2]/div[3]/label",
        )

        assert "Faltan campos por rellenar" == error.text

    def test_crear_mascota_error2(self):
        self.login()
        self.waitFor(self.botonMascotas)
        self.driver.find_element(*self.botonMascotas).click()
        self.waitFor(self.botonCrearMascota)
        self.driver.find_element(*self.botonCrearMascota).click()
        self.waitFor(self.botonSubirFoto)
        foto = self.driver.find_element(*self.botonSubirFoto)
        abs_path = os.path.abspath("test\integration\images\conejo.jpg")
        foto.send_keys(abs_path)
        self.waitFor(self.botonSubirFoto)
        self.driver.find_element(By.ID, "nombre").send_keys("Telma")
        raza = Select(self.driver.find_element(By.ID, "raza"))
        raza.select_by_value("Conejo")
        self.driver.find_element(By.ID, "descripcion").send_keys(
            "Lo más mono que he visto nunca"
        )
        self.driver.find_element(
            By.CSS_SELECTOR,
            "html > div > div.container > div.mascota > div.boton2 > button",
        ).click()
        error = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-anadir-mascota/html/div/div[2]/div[2]/div[1]/label",
        )

        assert "La imagen debe ser .png" in error.text


if __name__ == "__main__":
    unittest.main()
