import unittest
from test_init import InitTest
from selenium.webdriver.common.by import By


class TestValoracion(InitTest):

    def test_like(self):
        self.login()
        self.waitFor(
            (
                By.XPATH,
                "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/img",
            )
        )
        nlikes = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/div/div[1]/h2",
        ).text
        botonLike = self.driver.find_element(
            By.CSS_SELECTOR,
            "html > div > div.container > div.fotos > div:nth-child(1) > div > div:nth-child(1) > svg",
        )
        botonLike.click()
        n_likes_after = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/div/div[1]/h2",
        ).text

        dif = int(n_likes_after) - int(nlikes)
        assert dif == 1 or dif == -1

    def test_dislike(self):
        self.login()
        self.waitFor(
            (
                By.XPATH,
                "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/img",
            )
        )
        nlikes = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/div/div[2]/h2",
        ).text
        botonLike = self.driver.find_element(
            By.CSS_SELECTOR,
            "html > div > div.container > div.fotos > div:nth-child(1) > div > div:nth-child(2) > svg",
        )
        botonLike.click()
        n_likes_after = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/div/div[2]/h2",
        ).text

        dif = int(n_likes_after) - int(nlikes)
        assert dif == 1 or dif == -1


if __name__ == "__main__":
    unittest.main()
