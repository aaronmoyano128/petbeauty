import unittest
from test_init import InitTest
from selenium.webdriver.common.by import By


class TestValoracion(InitTest):

    def test_ordenadas(self):
        self.login()
        self.waitFor(
            (
                By.XPATH,
                "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/img",
            )
        )
        ordenadas = True
        nlikes = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/div/div[1]/h2",
        )
        i = 1
        div = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]",
        )
        contained_divs = div.find_elements(By.TAG_NAME, "div")
        nlikes = nlikes.text
        n = len(contained_divs) / 4
        while ordenadas and i <= n:
            i += 1
            newLike = self.driver.find_element(
                By.XPATH,
                f"/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[{i}]/div/div[1]/h2",
            ).text

            ordenadas = ordenadas and int(nlikes) >= int(newLike)
            nlikes = newLike

        assert ordenadas

    def test_filtar_ok(self):
        self.login()
        self.waitFor(
            (
                By.XPATH,
                "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/img",
            )
        )
        inpute = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[2]/input",
        )
        inpute.send_keys("pablito-mascotas")
        ordenadas = True
        all_user = True
        nlikes = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/div/div[1]/h2",
        )
        i = 1
        div = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[3]",
        )
        contained_divs = div.find_elements(By.TAG_NAME, "div")
        nlikes = nlikes.text
        n = len(contained_divs) / 4
        while ordenadas and i < n:
            i += 1
            newLike = self.driver.find_element(
                By.XPATH,
                f"/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[{i}]/div/div[1]/h2",
            ).text

            ordenadas = ordenadas and int(nlikes) >= int(newLike)
            nlikes = newLike

        assert ordenadas and all_user

    def test_filtar_nouser(self):
        self.login()
        self.waitFor(
            (
                By.XPATH,
                "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/img",
            )
        )
        inpute = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[2]/input",
        )
        inpute.send_keys("nonexistenuser")
        ordenadas = True
        all_user = True

        assert False

    def test_filtar_nouser(self):
        self.login()
        self.waitFor(
            (
                By.XPATH,
                "/html/body/app-root/app-explorar/html/div/div[2]/div[3]/div[1]/img",
            )
        )
        inpute = self.driver.find_element(
            By.XPATH,
            "/html/body/app-root/app-explorar/html/div/div[2]/div[2]/input",
        )
        inpute.send_keys("DavidMuñoz")
        ordenadas = True
        all_user = True

        assert False


if __name__ == "__main__":
    unittest.main()
