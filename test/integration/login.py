import unittest
from test_init import InitTest
from selenium.webdriver.common.by import By

class TestLogin(InitTest):

    def test_login_correcto(self):
        userBox = self.driver.find_element(By.ID, "username")
        userBox.send_keys("pablito-mascotas")
        passwordBox = self.driver.find_element(By.ID, "password")
        passwordBox.send_keys("contraseña123")
        boton = self.driver.find_element(By.XPATH, "/html/body/app-root/app-login/html/div/div/div[4]/button")
        boton.click()

        self.waitFor((By.XPATH, "/html/body/app-root/app-mis-mascotas/html/div/div[1]/app-navigation/html/div/div/div[6]"))

        assert "http://localhost:4200/mascotas" in self.driver.current_url

    def test_login_incorrecto1(self):

        userBox = self.driver.find_element(By.ID, "username")
        userBox.send_keys("alessia")
        passwordBox = self.driver.find_element(By.ID, "password")
        passwordBox.send_keys("contraseña123")
        boton = self.driver.find_element(By.XPATH, "/html/body/app-root/app-login/html/div/div/div[4]/button")
        boton.click()
        
        self.waitFor((By.CSS_SELECTOR, "#error"))
        error = self.driver.find_element(By.CSS_SELECTOR, "#error")
        assert error.text == "El usuario no existe."

    def test_login_incorrecto2(self):

        userBox = self.driver.find_element(By.ID, "username")
        userBox.send_keys("pablito-mascotas")
        passwordBox = self.driver.find_element(By.ID, "password")
        passwordBox.send_keys("contraseñaErronea")
        boton = self.driver.find_element(By.XPATH, "/html/body/app-root/app-login/html/div/div/div[4]/button")
        boton.click()
        
        self.waitFor((By.CSS_SELECTOR, "#error"))
        error = self.driver.find_element(By.CSS_SELECTOR, "#error")
        assert error.text == "La contraseña es incorrecta."

if __name__ == "__main__":
    unittest.main()
