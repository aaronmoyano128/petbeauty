from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class InitTest(unittest.TestCase):

    def setUp(self):
        # Crear instancias de ChromeOptions y ChromeDriverService
        options = Options()
        options.add_argument(
            "--start-maximized"
        )  # Opcional: maximizar la ventana del navegador al abrir
        # options.add_argument("--headless")   # Descomentar esta linea para que no se abra el Chrome (los test pasarán más rápido)
        service = Service(ChromeDriverManager().install())
        # Crear instancia del driver de Chrome con las opciones y el servicio
        self.driver = webdriver.Chrome(service=service, options=options)

        # Abre la página web localhost:4200
        self.driver.get("http://localhost:4200")
        return self.driver

    def login(self):
        # Ejemplo de interacción: buscar un elemento y enviar texto
        element = self.driver.find_element(By.ID, "username")
        element.send_keys("pablito-mascotas")

        # Ejemplo de interacción: buscar un elemento y enviar texto
        element = self.driver.find_element(By.ID, "password")
        element.send_keys("contraseña123")

        # Ejemplo de interacción: buscar un elemento y hacer clic
        element = self.driver.find_element(
            By.XPATH, "/html/body/app-root/app-login/html/div/div/div[5]/button"
        )
        element.click()

    def waitFor(self, locator, timeout=10):
        """
        Función que espera a que el elemento locator se cargue en la pagina.
        Usala cada vez que cambies de página para esperar hasta que esta cargue
        antes de hacer clicks o rellenar campos.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located(locator)
            )
            return element
        except Exception as e:
            print(f"Error al esperar el elemento: {e}")
            return None

    def tearDown(self):
        self.driver.quit()
