# PetBeautyy

## Starting up the service
To start up the backend and the database of petbeauty, you can use docker (and its recommended):
1. Execute ```docker compose up```, add the flag ```--build``` to rebuild the backend in case its updated (if not you will keep the old image).
2. The services will be started up in localhost (8000 for the backend, look at the backend README for more info and 5432 for database).
3. If you want to start up only the database ```docker compose up db```.

## Getting started

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
