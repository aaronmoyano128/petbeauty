-- Database schema for petbeauty

-- USE pet-beauty-db;
-- CREATE DATABASE pet-beauty-db;
-- \c pet-beauty-db;
-- Enums
CREATE TYPE user_permits AS ENUM ('user', 'admin');
CREATE TYPE pet_species AS ENUM ('deleted', 'Pájaro', 'Perro', 'Gato', 'Conejo', 'Tortuga', 'Rana', 'Ratón', 'Pez');

-- Tables
CREATE TABLE photo (
    photo_id SERIAL,
    photo_name VARCHAR(50),
    photo_media_type VARCHAR(50),
    photo bytea CHECK (photo is NULL OR photo_media_type is NOT NULL),
    PRIMARY KEY(photo_id)
);
INSERT INTO photo (photo_name) VALUES ('[NO PHOTO]');

CREATE TABLE app_user (
    user_id SERIAL,
    photo_id INT DEFAULT 1 NOT NULL,
    name VARCHAR(10) NOT NULL,
    surnames VARCHAR(40),
    username VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    password VARCHAR(60) NOT NULL,
    direction VARCHAR(100),
    description VARCHAR(100),
    user_type user_permits DEFAULT 'user' NOT NULL,
    is_verified BOOL DEFAULT FALSE,
    FOREIGN KEY(photo_id) REFERENCES photo(photo_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    PRIMARY KEY(user_id)
);
INSERT INTO app_user (name, username, email, password, user_type, is_verified) VALUES ('system', 'system-admin', 'system_petbeauty@gmail.com', 's3st3m-4dm1n', 'admin', TRUE);

CREATE TABLE pet (
    pet_id SERIAL,
    user_id INT DEFAULT 1 NOT NULL,
    photo_id INT DEFAULT 1 NOT NULL,
    name VARCHAR(10) NOT NULL,
    description VARCHAR(100),
    species pet_species NOT NULL,
    FOREIGN KEY(photo_id) REFERENCES photo(photo_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    FOREIGN KEY(user_id) REFERENCES app_user(user_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    PRIMARY KEY(pet_id)
);
INSERT INTO pet (name, description, species) VALUES ('[Deleted]', 'This pet was deleted by its owner.', 'deleted');

CREATE TABLE challenge (
    challenge_id SERIAL,
    name VARCHAR(30),
    description VARCHAR(100),
    start_date DATE DEFAULT CURRENT_DATE,
    end_date DATE,
    PRIMARY KEY(challenge_id)
);
INSERT into challenge (name, description, end_date) VALUES ('Daily', 'Daily Challenge (Default Option).', '9999-12-31');

CREATE TABLE publication (
    publication_id SERIAL,
    pet_id INT DEFAULT 1 NOT NULL,
    challenge_id INT DEFAULT 1 NOT NULL,
    photo_id INT DEFAULT 1 NOT NULL,
    publication_date DATE DEFAULT CURRENT_DATE,
    description VARCHAR(200) NOT NULL,
    FOREIGN KEY(photo_id) REFERENCES photo(photo_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    FOREIGN KEY(pet_id) REFERENCES pet(pet_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    FOREIGN KEY(challenge_id) REFERENCES challenge(challenge_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    PRIMARY KEY(publication_id)
);

CREATE TABLE vote (
    user_id INT NOT NULL,
    publication_id INT NOT NULL,
    FOREIGN KEY(user_id) REFERENCES app_user(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(publication_id) REFERENCES publication(publication_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE rating (
    user_id INT NOT NULL,
    publication_id INT NOT NULL,
    is_like BOOL NOT NULL,
    publication_date DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY(user_id) REFERENCES app_user(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(publication_id) REFERENCES publication(publication_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE comment (
    user_id INT NOT NULL,
    publication_id INT NOT NULL,
    text VARCHAR(200),
    publication_date DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY(user_id) REFERENCES app_user(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(publication_id) REFERENCES publication(publication_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE winner (
    challenge_id INT NOT NULL,
    publication_id INT NOT NULL,
    title VARCHAR(30),
    FOREIGN KEY(challenge_id) REFERENCES challenge(challenge_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(publication_id) REFERENCES publication(publication_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE advert (
    advert_id SERIAL,
    h_banner INT DEFAULT 1 NOT NULL,
    v_banner INT DEFAULT 1 NOT NULL,
    url VARCHAR(200) NOT NULL,
    company_name VARCHAR(50) NOT NULL,
    contact VARCHAR(100),
    FOREIGN KEY(h_banner) REFERENCES photo(photo_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    FOREIGN KEY(v_banner) REFERENCES photo(photo_id) ON UPDATE CASCADE ON DELETE SET DEFAULT,
    PRIMARY KEY(advert_id)
);

-- Load test data
CREATE OR REPLACE FUNCTION subir_fotos(directory_path TEXT)
RETURNS VOID AS 
$$
DECLARE
    file_record TEXT;
    photo_bytea BYTEA;
BEGIN
    -- Recorre todos los archivos en la carpeta
    FOR file_record IN 
        SELECT *
        FROM pg_ls_dir(directory_path)
        WHERE pg_ls_dir ~ '\.png$' -- Filtra solo archivos con extensión .png.
        ORDER BY 1
    LOOP
        -- Lee el archivo de imagen y conviértelo en bytea
        photo_bytea := pg_read_binary_file(directory_path || '/' || file_record);
        
        -- Inserta la foto en la tabla photo
        INSERT INTO photo (photo_name, photo_media_type, photo) 
        VALUES (file_record, 'image/png', photo_bytea);
        
        RAISE NOTICE 'Foto % insertada correctamente.', file_record;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

SELECT subir_fotos('/docker-entrypoint-initdb.d/test_data/photos/users');
SELECT subir_fotos('/docker-entrypoint-initdb.d/test_data/photos/pets');

COPY app_user(photo_id, name, username, email, password, user_type, is_verified) FROM '/docker-entrypoint-initdb.d/test_data/users.csv' DELIMITER ',' CSV HEADER;
COPY pet(photo_id, name, user_id, description, species) FROM '/docker-entrypoint-initdb.d/test_data/pets.csv' DELIMITER ',' CSV HEADER;

SELECT subir_fotos('/docker-entrypoint-initdb.d/test_data/photos/publications');

COPY publication(pet_id, photo_id, publication_date, description) FROM '/docker-entrypoint-initdb.d/test_data/publications.csv' DELIMITER ',' CSV HEADER;

COPY rating(user_id, publication_id, is_like, publication_date) FROM '/docker-entrypoint-initdb.d/test_data/ratings.csv' DELIMITER ',' CSV HEADER;