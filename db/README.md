# Database
We will be using the PostGres relational database.

To use the command line:
For mac install the brew command line (Windows look it up yourself)
```bash
brew install postgresql
```

## Start up the database
Just use the command:
```docker
docker compose up
```

## Restart the database completely
```docker
docker compose down
docker volume rm petbeauty_pet-beauty-data
```

## Add new test cases to the database
Modify the pets.csv, users.csv. If you want to add more test cases, in petbeauty.sql, look at the last lines for an example.

## Query the database
```bash
psql postgresql://pet-beauty-user:sj6LRhYn6Ns1c8xjW8z5@localhost:5432/pet-beauty-db
```
And now you are inside the database and can execute postgres commands such as:

```postgre
-- All tables and sequences on the database (contains the names of the ENUMS)
\d
-- All tables in the database 
\dt
\dt+
-- Description of the table
\d table_name
-- Users of the database
SELECT * from user;
-- To obtain the enum fields
select enum_range(NULL::app_user_permits)
SELECT unnest(enum_range(NULL::myenum))
SELECT unnest(enum_range(NULL::app_user_permits))::text
```