"""
Este es el módulo principal del proyecto.
"""

import uvicorn
from apis import pets, photos, publication, users
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.include_router(photos.router)
app.include_router(users.router)
app.include_router(pets.router)
app.include_router(publication.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
async def root():
    return "healthy"
 
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
