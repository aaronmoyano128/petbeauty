"""
    Upload and download of photos in the database.
"""
from api_classes import Publication, Species
from .utils import connection_config, check_ids, USER_EXIST, PET_EXIST, PHOTO_EXIST, check_str_len
from psycopg2 import DatabaseError, connect
from psycopg2.extras import NamedTupleCursor

def get_all_publications():
    publications_info = []
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT * FROM publication;")
                rows = cur.fetchall()
                for row in rows:
                    publication_info = {
                        "publication_id": row[0],
                        "pet_id": row[1],
                        "challenge_id": row[2],
                        "photo_id": row[3],
                        "description": row[5],
                        "publication_date": row[4],
                    }
                    publications_info.append(publication_info)
    except DatabaseError as error:
        print(error)
    return publications_info

def get_all_ratings():
    ratings_info = []
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT * FROM rating;")
                rows = cur.fetchall()
                for row in rows:
                    rating_info = {
                        "user_id": row[0],
                        "publication_id": row[1],
                        "is_like": row[2],
                        "publication_date": row[3],
                    }
                    ratings_info.append(rating_info)
    except DatabaseError as error:
        print(error)
    return ratings_info

def valid_publication(publication: Publication):
    # Validamos cada aspecto de la publicación y retornamos códigos de error específicos
    if not check_ids(USER_EXIST, (publication.user_id,)):
        return -2
    if not check_ids(PET_EXIST, (publication.pet_id,)):
        return -3
    if not check_ids(PHOTO_EXIST, (publication.photo_id,)):
        return -4
    if not check_str_len(publication.description, 200):
        return -5
    # Si todo es válido, no hay error
    return 1

CREATE_PUBLICATION = "INSERT INTO publication (pet_id, photo_id, description) VALUES (%s, %s, %s) RETURNING publication_id;"
def create_publication(publication: Publication) -> int:
    pet_id = publication.pet_id
    photo_id = publication.photo_id
    description = publication.description
    # Validamos la publicación y obtenemos el resultado de la validación
    valid_result = valid_publication(publication)
    # Si el resultado de la validación es negativo, retornamos el código de error
    if valid_result < 0:
        return valid_result

    publication_id = -1
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                # Ejecutar la consulta para crear la publicación
                cur.execute(CREATE_PUBLICATION, (pet_id, photo_id, description))
                row = cur.fetchone()
                if row:
                    publication_id = row[0]
                conn.commit()
    except DatabaseError as error:
        print(error)
    return publication_id

DELETE_PUBLICATION = "DELETE FROM publication WHERE publication_id = %s;"
def delete_publication(publication_id: int):
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                cur.execute(DELETE_PUBLICATION, (publication_id,))
                conn.commit()
    except DatabaseError as error:
        print(error)
        return False
    return True

RATING_CHECK = "SELECT is_like FROM rating WHERE user_id = %s AND publication_id = %s;"
def current_rating(user_id:int, publication_id: int):
    try:
        # Establecer conexión a la base de datos
        with connect(**connection_config) as conn:
            # Crear un cursor para ejecutar consultas SQL
            with conn.cursor() as cur:
                # Ejecutar la consulta para verificar la existencia del usuario
                cur.execute(RATING_CHECK, (user_id, publication_id))
                # Obtener el resultado de la consulta
                return cur.fetchone()
    except DatabaseError as error:
        print(error)
        # En caso de error, devolver 0
        return False

REMOVE_RATING = "DELETE FROM rating WHERE user_id = %s AND publication_id = %s;"
ADD_RATING = "INSERT INTO rating (user_id, publication_id, is_like) VALUES (%s, %s, %s) RETURNING publication_id;"
def add_rating(user_id:int, publication_id: int, is_like:bool):
    rating = current_rating(user_id, publication_id)
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                if rating is not None:
                    cur.execute(REMOVE_RATING, (user_id, publication_id))
                if rating is None or rating[0] != is_like:
                    cur.execute(ADD_RATING, (user_id, publication_id, is_like))
                conn.commit()
    except DatabaseError as error:
        print(error)
        return False
    return True

# Inicio de la query que se utiliza para obtener todas las publicaciones junto con su numero de likes y dislikes
START_ALL_PHOTOS = (
    "SELECT pub.publication_id, pub.photo_id, pub.publication_date, u.username, pet.species, "
    "COALESCE(SUM(CASE WHEN r.is_like THEN 1 ELSE 0 END - CASE WHEN NOT r.is_like THEN 1 ELSE 0 END), 0) AS global_rating, "
    "COALESCE(SUM(CASE WHEN r.is_like THEN 1 ELSE 0 END), 0) AS num_likes, "
    "COALESCE(SUM(CASE WHEN NOT r.is_like THEN 1 ELSE 0 END), 0) AS num_dislikes "
    "FROM publication pub JOIN pet ON pub.pet_id = pet.pet_id "
    "JOIN app_user u ON pet.user_id = u.user_id "
    "LEFT JOIN rating r ON pub.publication_id = r.publication_id "
    "WHERE "
)   
# Fragmento opcional de la query anterior que se añade UNICAMENTE si se filtra por mascota
IF_PET_FILTER = (
    "pet.species = %s AND "
)
# Final de la query anterior, esta parte SIEMPRE se debe ejecutar
END_ALL_PHOTOS = (
    "lower(u.username) LIKE lower(%s) GROUP BY pub.publication_id, u.username, pet.species "
    "ORDER BY global_rating DESC, num_likes DESC, pub.publication_date DESC;"
)
def get_most_liked_publications(species:str, username: str):
    username += '%'
    pubs = []
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                if species and species in [especie.value for especie in Species]:
                    cur.execute(START_ALL_PHOTOS+IF_PET_FILTER+END_ALL_PHOTOS, (species, username,))
                else:
                    cur.execute(START_ALL_PHOTOS+END_ALL_PHOTOS, (username,))
                rows = cur.fetchall()
                if rows:
                    pubs = [row._asdict() for row in rows]

    except DatabaseError as error:
        print(error)
        return pubs
    return pubs

USER_LIKED = (
    "SELECT r.is_like "
    "FROM rating r "
    "WHERE r.user_id = %s AND r.publication_id = %s;"
)
def user_liked(user_id: int, publication_id:int):
    liked = None
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(USER_LIKED, (user_id, publication_id,))
                row = cur.fetchone()
                if row:
                    liked = row.is_like
    except DatabaseError as error:
        print(error)
        return liked
    return liked