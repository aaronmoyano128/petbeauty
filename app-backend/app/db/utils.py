"""
Modulo de configuracion.
"""

from configparser import ConfigParser
from psycopg2 import DatabaseError, connect
from typing import Tuple
def load_config(filename: str = "database.ini", section="postgresql"):
    parser = ConfigParser()
    parser.read(filename)

    # get section, default to postgresql
    config = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            config[param[0]] = param[1]
    else:
        raise FileNotFoundError(f"Section {section} not found in the {filename} file")
    return config


connection_config = load_config()

USER_EXIST = "SELECT EXISTS (SELECT 1 FROM app_user WHERE user_id = %s);"
PET_EXIST = "SELECT EXISTS (SELECT 1 FROM pet WHERE pet_id = %s);"
PHOTO_EXIST = "SELECT EXISTS (SELECT 1 FROM photo WHERE photo_id = %s);"
def check_ids(select_check:str, ids:Tuple[int]):
    try:
        # Establecer conexión a la base de datos
        with connect(**connection_config) as conn:
            # Crear un cursor para ejecutar consultas SQL
            with conn.cursor() as cur:
                # Ejecutar la consulta para verificar la existencia del usuario
                cur.execute(
                    select_check,
                    ids,
                )
                # Obtener el resultado de la consulta
                exists = cur.fetchone()[0]
                # Devolver 1 si el usuario existe y 0 si no existe
                return True if exists else False
    except DatabaseError as error:
        print(error)
        # En caso de error, devolver 0
        return False

def check_str_len(str_value: str, str_len: int):
    return "" if not str_value else str_value if len(str_value) <= str_len else None
