"""
Aqui estan las principales llamadas a la base de datos sobre usuarios.
"""

from .utils import connection_config, check_ids, USER_EXIST, check_str_len
from api_classes import User
from psycopg2 import DatabaseError, connect
from psycopg2.extras import NamedTupleCursor
import bcrypt

def hash_password(password: str):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf8')

def check_password(password: str, hashed_password: str):
    # Check hashed password. Using bcrypt, the salt is saved into the hash itself
    return bcrypt.checkpw(password.encode('utf-8'), hashed_password.encode('utf-8'))

USER_QUERY = "SELECT user_id, email, password from app_user where username=%s;"
def get_user(username: str):
    user = None
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(USER_QUERY, (username,))
                row = cur.fetchone()
                if row:
                    user = row._asdict()
    except DatabaseError as error:
        print(error)
    return user

CREATE_SIMPLE_USER = "INSERT INTO app_user (name, username, email, password) VALUES (%s, %s, %s, %s) RETURNING user_id;"
CREATE_USER = (
    "INSERT INTO app_user (photo_id, name, surnames, username, email, password, direction, description) "
    "VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING user_id;"
)

def _user_values(user: User):
    return (
        1 if user.photo_id is None or user.photo_id < 1 else user.photo_id,
        check_str_len(user.name, 10),
        check_str_len(user.surnames, 40),
        check_str_len(user.username, 20),
        check_str_len(user.email, 30),
        check_str_len(user.password, 60),
        check_str_len(user.direction, 100),
        check_str_len(user.description, 100),
    )
    
def create_user(user: User):
    user_id = -1
    if not user.username or not user.password or not user.email:
        return user_id
    user.password = hash_password(user.password)
    user_values = _user_values(user)
    if any(v is None for v in user_values):
        return user_id
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(CREATE_USER, user_values)
                row = cur.fetchone()
                if row:
                    user_id = row[0]
                conn.commit()
    except DatabaseError as error:
        print(error)
    return user_id

PHOTOS_USER_QUERY = (
    "SELECT DISTINCT pub.publication_id, pub.pet_id, pub.challenge_id, pub.photo_id, pub.publication_date, "
    "pub.description FROM publication pub JOIN pet pe ON pub.pet_id = pe.pet_id "
    "JOIN photo p ON pub.photo_id = p.photo_id JOIN app_user u ON pe.user_id = u.user_id "
    "WHERE u.user_id = %s AND lower(pe.name) LIKE lower(%s) ORDER BY pub.publication_date DESC;"
)
def get_pub_user(user_id: int, pet_name:str):
    pub = []
    pet_name += '%'
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(PHOTOS_USER_QUERY, (user_id, pet_name))
                rows = cur.fetchall()
                if rows:
                    pub = [row._asdict() for row in rows]
    except DatabaseError as error:
        print(error)
    return pub


def valid_user(user_id: int):
    return check_ids(USER_EXIST, (user_id,))

GET_USER = (
    "SELECT photo_id, name, surnames, username, email, direction, description, is_verified "
    "FROM app_user WHERE user_id = %s;"
)
def user_info(user_id: int):
    if not valid_user(user_id):
        return None
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(GET_USER, (user_id,))
                row = cur.fetchone()
                user = row._asdict()
    except DatabaseError as error:
        print(error)
    return user


