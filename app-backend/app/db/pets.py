"""
Aqui estan las principales llamadas a la base de datos sobre mascotas.
"""

# Include here all the pet SQL operations

from api_classes import Pet, Species
import db.users as users
from .utils import connection_config, check_ids, PHOTO_EXIST, check_str_len
from psycopg2 import DatabaseError, connect
from psycopg2.extras import NamedTupleCursor

PET_QUERY = (
    "SELECT pet_id, name, description, species, photo_id FROM pet WHERE user_id=%s;"
)
PET_JOIN_PHOTO_QUERY = (
    "SELECT DISTINCT pet.pet_id, pet.name, pet.description, "
    "pet.species, photo.photo_id FROM pet JOIN photo ON "
    "pet.photo_id=photo.photo_id WHERE pet.user_id=%s;"
)
# Lista de mascotas (id, name, description, species(pet_species),photo) del usuario
def get_pets_joined_photo(user: int):
    pets = []
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(PET_JOIN_PHOTO_QUERY, (user,))
                rows = cur.fetchall()
                if rows:
                    for row in rows:
                        pets.append(row._asdict())
    except DatabaseError as error:
        print(error)
    return pets


def get_all_pet_names():
    pets_info = []
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT * FROM pet;")
                rows = cur.fetchall()
                for row in rows:
                    pet_info = {
                        "pet_id": row[0],
                        "user_id": row[1],
                        "photo_id": row[2],
                        "name": row[3],
                        "description": row[4],
                        "species": row[5],
                    }
                    pets_info.append(pet_info)
    except DatabaseError as error:
        print(error)
    return pets_info


def create_pet(mascota: Pet):
    user_id = mascota.user_id
    pet_name = mascota.pet_name
    photo_id = mascota.photo_id
    species = mascota.species
    description = mascota.description

    # comprobar si la mascota es válida
    if not valid(mascota) == 1:
        return valid(mascota)

    pet_id = -1

    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "INSERT INTO pet (user_id, photo_id, name, description, species) "
                    "VALUES (%s, %s, %s, %s, %s) RETURNING pet_id;",
                    (user_id, photo_id, pet_name, description, species),
                )
                row = cur.fetchone()
                if row:
                    pet_id = row[0]
                conn.commit()

    except DatabaseError as error:
        print(error)
    return pet_id


def valid(mascota: Pet):
    user_id = mascota.user_id
    pet_name = mascota.pet_name
    photo_id = mascota.photo_id
    species = mascota.species
    description = mascota.description

    if not users.valid_user(user_id):
        return -2
    if not check_str_len(pet_name, 10):
        return -3
    if not check_ids(PHOTO_EXIST, (photo_id,)):
        return -4
    if species not in [especie.value for especie in Species]:
        return -5
    if not check_str_len(description, 100):
        return -6

    return 1


def delete_pet(pet_name: int):
    try:
        with connect(**connection_config) as conn:
            with conn.cursor() as cur:
                cur.execute("DELETE FROM pet WHERE name = %s;", (pet_name,))
                conn.commit()
    except DatabaseError as error:
        print(error)
        return False
    return True

GET_PET = (
    "SELECT pet_id, photo_id, name, description, species "
    "FROM pet WHERE pet_id = %s;"
)
def getPet(pet_id: int):
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(GET_PET, (pet_id,))
                row = cur.fetchone()
                pet = row._asdict()
    except DatabaseError as error:
        print(error)
    return pet