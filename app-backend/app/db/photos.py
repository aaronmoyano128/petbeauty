"""
    Upload and download of photos in the database.
"""

from .utils import connection_config
from fastapi import UploadFile
from psycopg2 import Binary, DatabaseError, connect
from psycopg2.extras import NamedTupleCursor

UPLOAD_PHOTO = "INSERT INTO photo (photo_name, photo_media_type, photo) VALUES (%s, %s, %s) RETURNING photo_id;"
GET_PHOTO = "SELECT photo_media_type, photo from photo WHERE photo_id=%s;"


def upload_photo(photo: UploadFile):
    photo_id = -1
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                file_name = photo.filename
                content_type = photo.content_type
                file_contents = photo.file.read()
                cur.execute(
                    UPLOAD_PHOTO, (file_name, content_type, Binary(file_contents))
                )
                row = cur.fetchone()
                if row:
                    photo_id = row[0]
                conn.commit()
    except DatabaseError as error:
        print(error)
    return photo_id


def download_photo(photo_id: int):
    content = b""
    media_type = ""
    try:
        with connect(**connection_config) as conn:
            with conn.cursor(cursor_factory=NamedTupleCursor) as cur:
                cur.execute(GET_PHOTO, (photo_id,))
                row = cur.fetchone()
                if row:
                    media_type = row[0]
                    content = row[1].tobytes()
    except DatabaseError as error:
        print(error)
    return content, media_type
