"""
Aqui se encuentran las clases para la aplicación.
"""
from dataclasses import dataclass
from enum import Enum

@dataclass
class User:
    photo_id: int
    name: str
    surnames: str
    username: str
    email: str
    password: str
    direction: str
    description: str

@dataclass
class Publication:
    user_id: int
    pet_id: int
    photo_id: int
    description: str


@dataclass
class Pet:
    user_id: int
    pet_name: str
    photo_id: int
    species: str
    description: str


@dataclass
class Species(Enum):
    deleted = "deleted"
    pajaro = "Pájaro"
    perro = "Perro"
    gato = "Gato"
    conejo = "Conejo"
    tortuga = "Tortuga"
    rana = "Rana"
    raton = "Ratón"
    pez = "Pez"

