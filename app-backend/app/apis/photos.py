"""
Aqui estan las principales APIs relacionadas con las fotos.
"""

import db.photos as photos
from fastapi import APIRouter, File, HTTPException, Path, UploadFile
from fastapi.responses import Response
from typing_extensions import Annotated

router = APIRouter(
    prefix="/photos", tags=["photos"], responses={404: {"description": "Not found"}}
)


@router.post("/upload")
async def upload_photo(
    photo_file: Annotated[
        UploadFile, File(description="Photo to be uploaded to the database")
    ]
):
    photo_id = photos.upload_photo(photo_file)
    if photo_id == -1:
        raise HTTPException(404, "Photo was not saved correctly")
    return photo_id


@router.get("/download/{photo_id}")
async def download_photo(
    photo_id: Annotated[
        int,
        Path(
            description="Photo ID to be retrieved. The ID=1 is there is no photo", ge=1
        ),
    ]
):
    if photo_id == 1:
        raise HTTPException(400, "This ID has no photo associated")
    content, media_type = photos.download_photo(photo_id)
    return Response(content=content, media_type=media_type)


@router.get("/")
async def health():
    return "healthy"
