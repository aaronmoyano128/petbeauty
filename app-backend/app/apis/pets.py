"""
Aqui estan las principales APIs relacionadas con las mascotas.
"""

import api_classes as classes
import db.pets as pets
import db.users as users
from fastapi import APIRouter, HTTPException, Path
from typing_extensions import Annotated

router = APIRouter(
    prefix="/pets", tags=["pets"], responses={404: {"description": "Not found"}}
)

# Recibe el ID del usuario como parámetro del path y devuelve todas las mascotas asociadas a este.
@router.get("/listPets/{user}")
async def list_pets(user: Annotated[int, Path(description="User ID")]):
    if not users.valid_user(user):
        raise HTTPException(status_code=404, detail="User not found")
    pets_aux = pets.get_pets_joined_photo(user)
    res = []
    for pet in pets_aux:
        if pet["photo_id"]:
            res.append(
                {
                    "pet_id": pet["pet_id"],
                    "name": pet["name"],
                    "description": pet["description"],
                    "species": pet["species"],
                    "photo_id": pet["photo_id"],
                }
            )
    return res


@router.get("/get/")
async def obtener_mascotas():
    mascotas = pets.get_all_pet_names()
    return mascotas


@router.post("/createPet/")
async def crear_mascota(mascota: classes.Pet):

    pet_id = pets.create_pet(mascota)
    if pet_id == -1:
        raise HTTPException(404, "Error when executing query")
    elif pet_id == -2:
        raise HTTPException(404, "user_id provided not existent")
    elif pet_id == -3:
        raise HTTPException(404, "pet_name provided is invalid")
    elif pet_id == -4:
        raise HTTPException(404, "photo_id provided is invalid")
    elif pet_id == -5:
        raise HTTPException(404, "species provided is invalid")
    elif pet_id == -6:
        raise HTTPException(404, "description provided is invalid")

    return {"Pet creation success"}


@router.delete("/deletePet/{pet}")
async def delete_pet(pet: Annotated[str, Path(description="Pet Name")]):
    pets.delete_pet(pet)
    return {"detail": "Pet deleted"}


@router.get("/getPet/{pet_id}")
async def getPet(pet_id: Annotated[int, Path(user_id="Id usuario")]):
    return pets.getPet(pet_id)


@router.get("/")
async def health():
    return "healthy"
