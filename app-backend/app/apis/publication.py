"""
Aqui estan las principales APIs relacionadas con las publicaciones.
"""

from api_classes import Publication
import db.publication as pub
from fastapi import APIRouter, HTTPException, Path, Query
from typing_extensions import Annotated

router = APIRouter(
    prefix="/publication",
    tags=["publication"],
    responses={404: {"description": "Not found"}},
)


@router.get("/get/")
async def obtener_publicaciones():
    publicaciones = pub.get_all_publications()
    return publicaciones


@router.post("/subirPublicacion/")
async def subir_publicacion(publicacion: Publication):
    # se crea la publicacion
    publication_id = pub.create_publication(publicacion)
    if publication_id == -1:
        raise HTTPException(404, "Error when executing query")
    elif publication_id == -2:
        raise HTTPException(404, "user_id provided not existent")
    elif publication_id == -3:
        raise HTTPException(404, "pet_id provided is invalid")
    elif publication_id == -4:
        raise HTTPException(404, "photo_id provided is invalid")
    elif publication_id == -5:
        raise HTTPException(404, "description provided is invalid")

    # Devolver un mensaje HTTP de éxito
    return {"Publication creation success"}


@router.delete("/deletePublication/{id_publicacion}")
async def delete_publication(
    publication_id: Annotated[int, Path(publication_id="Id Publicacion")]
    ):
    deleted = pub.delete_publication(publication_id)
    return {"deleted": deleted}

@router.put("/anadirValoracion/{user_id}/{publication_id}")
async def add_rating(
    user_id: Annotated[int, Path(user_id="Id usuario")],
    publication_id: Annotated[int, Path(publication_id="Id Publicacion")],
    is_like: Annotated[bool, Query(is_like="Si el usuario dejo un like")]
    ):
    return {"modified": pub.add_rating(user_id, publication_id, is_like)}

@router.get("/mostLiked/{user_id}")
async def get_all_publications(
    user_id: Annotated[int, Path(publication_id="Id usuario")],
    species: Annotated[str, Query(publication_id="Especie para filtrar las imagenes")] = "",
    username: Annotated[str, Query(username="Nombre de usuario para filtrar las imagenes")] = ""
    ):
    ordered_publications = pub.get_most_liked_publications(species, username)
    for publication in ordered_publications:
        publication["you_liked"] = pub.user_liked(user_id, publication["publication_id"])
    return ordered_publications

@router.get("/getAllRatings/")
async def obtener_ratings():
    ratings = pub.get_all_ratings()
    return ratings

@router.get("/")
async def health():
    return "healthy"
