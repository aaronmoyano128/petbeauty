"""
Aqui estan las principales APIs relacionadas con los usuarios.
"""

import db.users as users
from api_classes import User
from fastapi import APIRouter, HTTPException, Path, Query, Body
from typing_extensions import Annotated
from typing import Union

router = APIRouter(
    prefix="/users", tags=["users"], responses={404: {"description": "Not found"}}
)

@router.get("/testhash/{text}")
async def test_hash(text:str):
    return users.hash_password(text)

@router.post("/createUser")
async def create_user(
        user: Annotated[User, Body(description="Username of the User trying to login")],
    ):
    user_id = users.create_user(user)
    if user_id == -1:
        raise HTTPException(status_code=400, detail="Missing user arguments or invalid field lengths")
    return {"user_id": user_id}

@router.get("/identity/{username}/{password}")
async def check_identity(
        username: Annotated[str, Path(description="Username of the User trying to login")],
        password: Annotated[str, Path(description="Password of the User")],
    ):
    user = users.get_user(username)
    if not user:
        raise HTTPException(status_code=403, detail="User not found")
    if not users.check_password(password, user['password']):
        raise HTTPException(status_code=404, detail="Invalid Password")
    return {"user_id": user["user_id"], "email": user["email"]}


# Crear un endpoint que reciba el id de un usuario y devuelva todas las fotos que este ha subido.
@router.get("/profilePhotos/{user_id}")
async def get_profile_photos(
    user_id: Annotated[int, Path(description="User ID of the User")],
    pet_name: Annotated[Union[str], Query(description="Name of a pet of the user")] = None
    ):
    user = users.valid_user(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    photos = users.get_pub_user(user_id, pet_name or "")
    return {"photos": photos}

@router.get("/userInfo/{user_id}")
async def user_info(
    user_id: Annotated[int, Path(description="User ID of the User")],
):
    user = users.user_info(user_id)
    if not user:
        raise HTTPException(status_code=403, detail="User not found")
    return {"user_info": user}

@router.get("/")
async def health():
    return "healthy"
