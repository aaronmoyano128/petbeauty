# App Backend

To start it up locally from petbeauty.
1. Install the dependencies in requirements.txt.

2. Run this command
```bash
python3 app-backend/main.py
```

3. Spin up the database for testing.
```bash
docker compose up db
```
4. You can check the apps documentation and APIs at ```http://localhost:8000/docs``` or ```http://localhost:8000/redoc```

5. To stop the api, cmd-C in the uvicorn terminal. cmd-C in the docker window and ```docker compose down db```

## app/apis
Contain the logic for the apis that we are implementing.
https://fastapi.tiangolo.com/tutorial/first-steps/

## app/db
Contains the logic for the interface between the database and the apis.
Look here for examples:
https://www.postgresqltutorial.com/postgresql-python/query/