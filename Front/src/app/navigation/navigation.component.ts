import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { PopUpPublicacionComponent } from '../pop-up-publicacion/pop-up-publicacion.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  @Input() pag?: number;
  userID: string = '0';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    if (this.pag) {
      // Al cargar la componente, debemos indicar en qué pagina estamos
      const pags: NodeListOf<HTMLDivElement> = document.querySelectorAll('.pag');
      pags[this.pag].classList.add("active");

      this.activatedRoute.params.subscribe(params => {
        this.userID = params['id'];
      })
    }
  }

  openPopup(): void {
    const popUp = this.dialog.open(PopUpPublicacionComponent, {
      width: '700px',
      height: '750px',
      data: { userID: this.userID },
      disableClose: false, // Deshabilita cerrar el diálogo al hacer clic fuera de él o al presionar la tecla Esc
      autoFocus: true // Autofoco en el primer elemento del diálogo modal
    })
    .afterClosed()
    .subscribe((shouldReload: boolean) => {
        popUp.unsubscribe();
        if (shouldReload) window.location.reload()
    });
  }

}
