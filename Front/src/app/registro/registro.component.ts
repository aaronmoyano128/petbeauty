import { Component, OnInit, Inject, ViewChild, ElementRef} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {
  username: string = '';
  mail: string = '';
  password: string = '';
  passwordconfirmation: string = '';
  formCorrect: boolean = false;
  paramsRegistro2: any[] = [];

  constructor(private router: Router) {}

  submitForm() {
    // Verificar que todos los campos estén completos
    if (!this.username || !this.mail || !this.password || !this.passwordconfirmation) {
      console.log("Por favor completa todos los campos.");
      return;
    }

    // Verificar que userpassword y passwordconfirmation sean iguales
    if (this.password !== this.passwordconfirmation) {
      console.log("Las contraseñas no coinciden.");
      return;
    }

    // Verificar el formato del correo electrónico
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!emailPattern.test(this.mail)) {
      console.log("El formato del correo electrónico es incorrecto.");
      return;
    }

    // Si pasa todas las verificaciones, establecer formCorrect en true y redirigir
    this.formCorrect = true;
    console.log("Formulario válido. ¡Continuar!");
    this.paramsRegistro2 = [
      { username: this.username, email: this.mail, password: this.password }
    ];

    // Redirigir a la página registro2 y pasar los parámetros
    this.router.navigate(['/registro2'], { state: { paramsRegistro2: this.paramsRegistro2 } });
  }

}
