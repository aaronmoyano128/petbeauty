import { Component, OnInit} from '@angular/core';
import { PopUpPublicacionComponent } from '../pop-up-publicacion/pop-up-publicacion.component';
import {MatDialog, MatDialogActions} from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
@Component({
  selector: 'app-crear-publicacion',
  templateUrl: './crear-publicacion.component.html',
  styleUrls: ['./crear-publicacion.component.css']
})
export class CrearPublicacionComponent implements OnInit{


  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
    this.abrirDialogo(); // Abre el diálogo modal al inicializar el componente
  }

  abrirDialogo(): void {
    this.dialog.open(PopUpPublicacionComponent, {
      width: '700px',
      height: '100px',
      data: { title: 'Nueva Publicación' },
      disableClose: true, // Deshabilita cerrar el diálogo al hacer clic fuera de él o al presionar la tecla Esc
      autoFocus: true // Autofoco en el primer elemento del diálogo modal
    });
  }
}

