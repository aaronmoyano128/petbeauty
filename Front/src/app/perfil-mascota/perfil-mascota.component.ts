import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { apiCall } from '../conection/apiRequest'
import { Publicacion } from '../conection/publicaciones';
import { Mascota } from '../conection/mascotas';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-perfil-mascota',
  templateUrl: './perfil-mascota.component.html',
  styleUrls: ['./perfil-mascota.component.css']
})
export class PerfilMascotaComponent {
  userID: string = '0';
  mascotaID: string = '0';
  publicaciones: Publicacion[] = []
  mascota: Mascota = {
    pet_id: 0,
    name: '',
    description: '',
    species: '',
    photo_id: 0,
    photo: ''
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userID = params['id'];
      this.mascotaID = params['idMascota'];

      this.apiCall.getMascota(this.mascotaID).subscribe(
        (mascota: Mascota) => {
          this.mascota = mascota;
          this.obtenerImagenesParaMascota();
          console.log(this.mascota)

          // Llama a la segunda función después de completar la primera solicitud
          this.apiCall.getProfileFotos(this.userID, this.mascota.name).subscribe(
            (publicaciones: Publicacion[]) => {
              this.publicaciones = publicaciones;
              this.obtenerImagenesParaPublicaciones();
              console.log(this.publicaciones)
            },
            (error: any) => {
              console.error('Error al obtener las publicaciones:', error);
            }
          );
        },
        (error: any) => {
          console.error('Error al obtener las mascotas:', error);
        }
      );
    });
  }

  obtenerImagenesParaMascota(): void {
    this.apiCall.obtenerImagen(this.mascota.photo_id).subscribe(
      (imagen: Blob) => {
        this.mascota.photo = URL.createObjectURL(imagen);
      },
      (error: any) => {
        console.error('Error al obtener la imagen para la mascota:', this.mascota.name, error);
      }
    );
  }

  obtenerImagenesParaPublicaciones(): void {
    this.publicaciones.forEach(publicacion => {
      this.apiCall.obtenerImagen(publicacion.photo_id).subscribe(
        (imagen: Blob) => {
          publicacion.photo = URL.createObjectURL(imagen);
        },
        (error: any) => {
          console.error('Error al obtener la imagen para la publicación:', publicacion.publication_id, error);
        }
      );
    });
  }

  dividirEnGruposDeTres(array: Publicacion[]): Publicacion[][] {
    const grupos = [];
    for (let i = 0; i < array.length; i += 3) {
      grupos.push(array.slice(i, i + 3));
    }
    return grupos;
  }

  mostrarId(id: number): void {
    console.log(id);
  }
}



