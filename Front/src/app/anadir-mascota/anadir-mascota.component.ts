import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { apiCall } from '../conection/apiRequest';

@Component({
  selector: 'app-anadir-mascota',
  templateUrl: './anadir-mascota.component.html',
  styleUrls: ['./anadir-mascota.component.css']
})
export class AnadirMascotaComponent {
  nombre: string = '';
  raza: string = '';
  descripcion: string = '';
  userID: string = '0';
  imagenSrc: string | ArrayBuffer | null = '../../assets/huella.jpg'; // Ruta de la imagen por defecto
  error: number = 0;
  id_imagen: number = 0;


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userID = params['id'];
    })
  }

   // Método para abrir el cuadro de diálogo para seleccionar un archivo
   openFileInput(): void {
    document.getElementById('fileInput')!.click();
  }

  // Método para actualizar la imagen de la mascota al seleccionar un archivo
  onFileSelected(event: any): void {
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (fileInput) {
      const file = fileInput.files?.[0];
      if (file) {
        const reader = new FileReader();
        reader.onload = () => {
          this.imagenSrc = reader.result;
        };
        reader.readAsDataURL(file);
      }
    }
  }

  // Función para manejar la carga de la foto y enviar los datos al servidor
  crearMascota(): void {
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (!fileInput) {
      alert('No se encontró el elemento fileInput.');
      return;
    }
    const file = fileInput.files?.[0];

    if((file && file.type !== 'image/png') && (!this.nombre || !this.raza || !this.descripcion)){
      this.error = 3
      return
    }
    if (file && file.type !== 'image/png') {
      this.error = 2
      return
    }

    if (!this.nombre || !this.raza || !this.descripcion || !file) {
      this.error = 1
      return;
    }

    this.apiCall.subirImagen(file).subscribe(
      (response) => {
        const body = {
          'user_id': parseInt(this.userID),
          'pet_name': this.nombre,
          'photo_id': response,
          'species': this.raza,
          'description': this.descripcion
        }

        this.apiCall.crearMascota(body).subscribe()

        this.router.navigate(['/mascotas/', this.userID]);
      },
      (error) => {
        console.error('Error en la subida del archivo:', error);
      }
    );
  }

}
