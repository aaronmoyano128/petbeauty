import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { apiCall } from '../conection/apiRequest'
import { Publicacion } from '../conection/publicaciones';
import { Subscription } from 'rxjs';
import { UserInfo } from '../conection/userInfo';
import { Mascota } from '../conection/mascotas';

@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.css']
})

export class PerfilUsuarioComponent {
  userID: string = '0';
  misPublicaciones: Publicacion[] = []
  user: UserInfo = {
    photo_id: undefined,
    name: undefined,
    surnames: undefined,
    username: '',
    email: '',
    direction: undefined,
    description: undefined,
    is_verified: false,
    photo: ''
  };
  misMascotas: Mascota[] = [];
  mascota: string | undefined = undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userID = params['id'];
      this.apiCall.getProfileFotos(this.userID).subscribe(
        (publicaciones: Publicacion[]) => {
          this.misPublicaciones = publicaciones;
          this.obtenerImagenesParaPublicaciones();
        },
        (error: any) => {
          console.error('Error al obtener las publicaciones:', error);
        }
      );
      this.apiCall.getUserInfo(this.userID).subscribe(
        (user: UserInfo) => {
          this.user = user;
          if (user.photo_id !== 1) {
            this.apiCall.obtenerImagen(this.user.photo_id).subscribe(
              (imagen: Blob) => {
                user.photo = URL.createObjectURL(imagen);
              },
              (error: any) => {
                console.error('Error al obtener la imagen para la publicación:', user.username, error);
              }
            );
          }
        },
        (error: any) => {
          console.error('Error al obtener las publicaciones:', error);
        }
      );
      this.apiCall.getMascotas(this.userID).subscribe(
        (mascotas: Mascota[]) => {
          this.misMascotas = mascotas;
        },
        (error: any) => {
          console.error('Error al obtener las mascotas:', error);
        }
      );
    });
  }

  obtenerImagenesParaPublicaciones(): void {
    this.misPublicaciones.forEach(publicacion => {
      this.apiCall.obtenerImagen(publicacion.photo_id).subscribe(
        (imagen: Blob) => {
          publicacion.photo = URL.createObjectURL(imagen);
        },
        (error: any) => {
          console.error('Error al obtener la imagen para la publicación:', publicacion.publication_id, error);
        }
      );
    });
  }

  dividirEnGruposDeTres(array: Publicacion[]): Publicacion[][] {
    const grupos = [];
    for (let i = 0; i < array.length; i += 3) {
      grupos.push(array.slice(i, i + 3));
    }
    return grupos;
  }

  filtrar(mascota?: string){
    this.apiCall.getProfileFotos(this.userID, mascota).subscribe(
      (publicaciones: Publicacion[]) => {
        this.misPublicaciones = publicaciones;
        this.obtenerImagenesParaPublicaciones();
      },
      (error: any) => {
        console.error('Error al obtener las publicaciones:', error);
      }
    );
  }

  mostrarId(id: number): void {
    console.log(id);
  }

}
