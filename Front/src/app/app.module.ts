import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogoComponent } from './logo/logo.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LoginComponent } from './login/login.component';
import { MisMascotasComponent } from './mis-mascotas/mis-mascotas.component';
import { AnadirMascotaComponent } from './anadir-mascota/anadir-mascota.component';
import { FormsModule } from '@angular/forms';

import { CrearPublicacionComponent } from './crear-publicacion/crear-publicacion.component';
import { PopUpPublicacionComponent } from './pop-up-publicacion/pop-up-publicacion.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { ExplorarComponent } from './explorar/explorar.component';
import { RegistroComponent } from './registro/registro.component';
import { Registro2Component } from './registro2/registro2.component';
import { VerificacionComponent } from './verificacion/verificacion.component';
import { RecuperacionComponent } from './recuperacion/recuperacion.component';
import { PerfilMascotaComponent } from './perfil-mascota/perfil-mascota.component';
@NgModule({
  declarations: [
    AppComponent,
    LogoComponent,
    NavigationComponent,
    LoginComponent,
    MisMascotasComponent,
    AnadirMascotaComponent,
    AnadirMascotaComponent,
    CrearPublicacionComponent,
    PopUpPublicacionComponent,
    PerfilUsuarioComponent,
    PerfilMascotaComponent,
    ExplorarComponent,
    RegistroComponent,
    Registro2Component,
    VerificacionComponent,
    RecuperacionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
