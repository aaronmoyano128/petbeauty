import { Component, Inject, OnInit } from '@angular/core';
import { apiCall } from '../conection/apiRequest'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  id: string = '0';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) {}

  iniciarSesion() {
    this.apiCall.login(this.username, this.password).subscribe(id_usuario => {
      this.id = id_usuario
      // Si id = -2, contraseña incorrecta, si id == -1, usuario no existe
      if (this.id != '-2' && this.id != '-1' && this.id != '0'){
        this.router.navigate(['/explorar/', this.id]);
      }
    });
  }
}
