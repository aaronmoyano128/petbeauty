import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { apiCall } from '../conection/apiRequest'
import { Mascota } from '../conection/mascotas';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-mis-mascotas',
  templateUrl: './mis-mascotas.component.html',
  styleUrls: ['./mis-mascotas.component.css']
})



export class MisMascotasComponent {
  userID: string = '0'; 
  misMascotas: Mascota[] = []

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) {}


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userID = params['id'];
      this.apiCall.getMascotas(this.userID).subscribe(
        (mascotas: Mascota[]) => {
          this.misMascotas = mascotas;
          this.obtenerImagenesParaMascotas();
        },
        (error: any) => {
          console.error('Error al obtener las mascotas:', error);
        }
      );
    })
  }

  obtenerImagenesParaMascotas(): void {
    this.misMascotas.forEach(misMascotas => {
      this.apiCall.obtenerImagen(misMascotas.photo_id).subscribe(
        (imagen: Blob) => {
          misMascotas.photo = URL.createObjectURL(imagen);
        },
        (error: any) => {
          console.error('Error al obtener la imagen para la mascota:', misMascotas.name, error);
        }
      );
    });
  }
}
