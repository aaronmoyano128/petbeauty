import { Component } from '@angular/core';
import { apiCall } from '../conection/apiRequest'
import { ActivatedRoute } from '@angular/router';
import { LikedPublicaciones } from '../conection/likedPublicaciones';

@Component({
  selector: 'app-explorar',
  templateUrl: './explorar.component.html',
  styleUrls: ['./explorar.component.css']
})
export class ExplorarComponent {
  userID: string = '0';
  publicaciones: LikedPublicaciones[] = []
  raza: string = '';
  usuario: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userID = params['id'];
      this.apiCall.getAllPublications(this.userID).subscribe(
        (publicaciones: LikedPublicaciones[]) => {
          this.publicaciones = publicaciones;
          this.obtenerImagenesParaPublicaciones();
        },
        (error: any) => {
          console.error('Error al obtener las publicaciones:', error);
        }
      );
    });
  }

  obtenerImagenesParaPublicaciones(): void {
    this.publicaciones.forEach(publicacion => {
      this.apiCall.obtenerImagen(publicacion.photo_id).subscribe(
        (imagen: Blob) => {
          publicacion.photo = URL.createObjectURL(imagen);
        },
        (error: any) => {
          console.error('Error al obtener la imagen para la publicación:', publicacion.publication_id, error);
        }
      );
    });
  }

  darLike(publication_id: number){
    var posicion = this.encontraPublicacion(publication_id)
    if(this.publicaciones[posicion].you_liked == false){
      this.publicaciones[posicion].num_dislikes--
      this.publicaciones[posicion].you_liked = true
      this.publicaciones[posicion].num_likes++
    } else if(this.publicaciones[posicion].you_liked == true) {
      this.publicaciones[posicion].you_liked = undefined
      this.publicaciones[posicion].num_likes--
    } else {
      this.publicaciones[posicion].you_liked = true
      this.publicaciones[posicion].num_likes++
    }
    this.apiCall.addRating(this.userID, publication_id.toString(), "true").subscribe()
  }
  darDislike(publication_id: number){
    var posicion = this.encontraPublicacion(publication_id)
    if(this.publicaciones[posicion].you_liked == true){
      this.publicaciones[posicion].num_likes--
      this.publicaciones[posicion].you_liked = false
      this.publicaciones[posicion].num_dislikes++
    } else if(this.publicaciones[posicion].you_liked == false){
      this.publicaciones[posicion].you_liked = undefined
      this.publicaciones[posicion].num_dislikes--
    } else {
      this.publicaciones[posicion].you_liked = false
      this.publicaciones[posicion].num_dislikes++
    }
    this.apiCall.addRating(this.userID, publication_id.toString(), "false").subscribe()
  }

  filtrar(username?: string, specie?: string){
    this.apiCall.getAllPublications(this.userID, username, specie).subscribe(
      (publicaciones: LikedPublicaciones[]) => {
        this.publicaciones = publicaciones;
        this.obtenerImagenesParaPublicaciones();
      },
      (error: any) => {
        console.error('Error al obtener las publicaciones:', error);
      }
    ); 
  }

  encontraPublicacion(publication_id: number){
    var i = 0;
    var encontrado = false;
    while(!encontrado){
      if(this.publicaciones[i].publication_id == publication_id){
        return i;
      }
      i++;
    }
    return 0;
  }
}
