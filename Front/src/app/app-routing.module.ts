import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MisMascotasComponent } from './mis-mascotas/mis-mascotas.component';
import { AnadirMascotaComponent } from './anadir-mascota/anadir-mascota.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { PopUpPublicacionComponent } from './pop-up-publicacion/pop-up-publicacion.component';
import { ExplorarComponent } from './explorar/explorar.component';
import { RegistroComponent } from './registro/registro.component';
import { Registro2Component } from './registro2/registro2.component';
import { VerificacionComponent } from './verificacion/verificacion.component';
import { RecuperacionComponent } from './recuperacion/recuperacion.component';
import { PerfilMascotaComponent } from './perfil-mascota/perfil-mascota.component';


//rutas de navegacion
const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'mascotas/:id', component: MisMascotasComponent},
  {path: 'nueva-mascota/:id', component: AnadirMascotaComponent},
  {path: 'perfilUsuario/:id', component:PerfilUsuarioComponent},
  {path: 'perfilMascota/:id/:idMascota', component:PerfilMascotaComponent},
  {path: 'explorar/:id', component:ExplorarComponent},
  {path: 'registro', component:RegistroComponent},
  {path: 'registro2', component:Registro2Component},
  {path: 'verificacion', component:VerificacionComponent},
  {path: 'recuperacion', component:RecuperacionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
