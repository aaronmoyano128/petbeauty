export class Publicacion {
    publication_id: number;
    pet_id: number;
    challenge_id: number;
    photo_id: number;
    publication_date: Date;
    description: string;
    photo: string;
  
    constructor(publication_id: number, pet_id: number, challenge_id: number, photo_id: number, publication_date: Date, description: string, photo: string) {
      this.publication_id = publication_id;
      this.pet_id = pet_id;
      this.challenge_id = challenge_id;
      this.photo_id = photo_id;
      this.publication_date = publication_date;
      this.description = description;
      this.photo = photo;
    }
  }