import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Mascota } from './mascotas';
import { Publicacion } from './publicaciones';
import { LikedPublicaciones } from './likedPublicaciones';
import { UserInfo } from './userInfo';
import { isLength } from 'html2canvas/dist/types/css/types/length';

const apiUrl = 'http://localhost:8000';

interface DatosUsuario {
  email: string;
  user_id: string;
}

@Injectable({
  providedIn: 'root'
})

export class apiCall {

  constructor(private http: HttpClient) {}
  
  login(user: string, password: string ): Observable<string> {
    return this.http.get<DatosUsuario>(
      apiUrl+'/users/identity/'+user+'/'+password
    ).pipe(
      map((datosUsuario: DatosUsuario) => datosUsuario.user_id),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 403) {
          return of("-1");
        } else if (error.status === 404) {
          return of("-2");
        } else {
          console.error('Error de HTTP:', error.message);
          throw error;
        }
      })
    );
  }

  obtenerImagen(id: number | undefined): Observable<Blob> {
    return this.http.get(
      apiUrl+"/photos/download/"+id, { responseType: 'blob' }
    ).pipe(
      catchError(error => {
        console.error('Error al descargar la imagen:', error);
        return throwError(error);
      })
    );
  }

  subirImagen(photo: File): Observable<number> {
    const formData = new FormData();
    formData.append('photo_file', photo, photo.name);

    return this.http.post<number>(
      apiUrl+'/photos/upload', formData
    ).pipe(
      catchError(error => {
        console.error('Error al descargar la imagen:', error);
        return throwError(error);
      })
    );
  }

  getMascotas(user_id: string): Observable<Mascota[]> {
    return this.http.get<Mascota[]>(
      apiUrl+'/pets/listPets/'+user_id
    ).pipe(
      catchError(error => {
        console.error('Error al descargar la imagen:', error);
        return throwError(error);
      })
    );
  }

  getMascota(pet_id: string): Observable<Mascota> {
    return this.http.get<Mascota>(
      apiUrl+'/pets/getPet/'+pet_id
    ).pipe(
      catchError(error => {
        console.error('Error al descargar la imagen:', error);
        return throwError(error);
      })
    );
  }

  getProfileFotos(user_id: string, mascota?: string): Observable<Publicacion[]>{
    let url = apiUrl + '/users/profilePhotos/' + user_id;
    if (mascota) {
      url += '?pet_name=' + mascota;
    }
    return this.http.get<Publicacion[]>(url).pipe(
      map((respuesta: any) => respuesta.photos)
    )
  }

  crearMascota(
    body: { user_id: number; pet_name: string; description: string; photo_id: number; species: string; }
  ): Observable<string> {
    return this.http.post<string>(
      apiUrl+'/pets/createPet/', body
    ).pipe(
      catchError(error => {
        console.error('Error al descargar la imagen:', error);
        return throwError(error);
      })
    );
  }

  crearPublicacion(
    body: { user_id: number; pet_id: number | undefined; photo_id: number; description: string; }
  ): Observable<string> {
    return this.http.post<string>(
      apiUrl+'/publication/subirPublicacion/', body
    ).pipe(
      catchError(error => {
        console.error('Error al descargar la imagen:', error);
        return throwError(error);
      })
    );
  }

  getAllPublications(user_id: string, username?: string, species?: string): Observable<LikedPublicaciones[]> {
    let url = apiUrl + '/publication/mostLiked/' + user_id;
    if (username || species) {
      url += '?';
      if (username) {
        url += 'username=' + username + '&';
      }
      if (species) {
        url += 'species=' + species;
      }
    }
    return this.http.get<LikedPublicaciones[]>(url).pipe(
      map((respuesta: any) => respuesta)
    );
  }

  addRating(user_id: string, publication_id: string, is_like?: string): Observable<any> {
    let url = apiUrl + '/publication/anadirValoracion/' + user_id + '/' + publication_id;
    if(is_like)
      url += '?is_like=' + is_like
    return this.http.put(url, {});
  }

  getUserInfo(id_usuario: string): Observable<any>{
    return this.http.get<UserInfo>(apiUrl+"/users/userInfo/"+id_usuario).pipe(
      map((respuesta: any) => respuesta.user_info)
    );
  }

  crearUsuario(
    body: { 
      photo_id: number; name: string; surnames: string;
      username: string; email: string; password: string;
      direction: string; description: string;
    }
  ): Observable<string> {
    return this.http.post<string>(
      apiUrl+'/users/createUser/', body
    ).pipe(
      catchError(error => {
        console.error('Error al crear el usuario:', error);
        return throwError(error);
      })
    );
  }
}
