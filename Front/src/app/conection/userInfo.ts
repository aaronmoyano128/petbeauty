export class UserInfo {
    photo_id: number | undefined;
    name: string | undefined;
    surnames: string | undefined;
    username: string;
    email: string;
    direction: string | undefined;
    description: string | undefined;
    is_verified: boolean;
    photo: string;
  
    constructor(
        photo_id: number | undefined, name: string | undefined, surnames: string | undefined,
        username: string, email: string, direction: string | undefined,
        description: string | undefined, is_verified: boolean, photo: string
    ) {
      this.photo_id = photo_id;
      this.name = name;
      this.surnames = surnames;
      this.username = username;
      this.email = email;
      this.direction = direction;
      this.description = description;
      this.is_verified = is_verified;
      this.photo = photo;
    }
}