export class LikedPublicaciones {
    publication_id: number;
    photo_id: number;
    publication_date: Date;
    username: string;
    specie: string;
    global_rating: number;
    num_likes: number;
    num_dislikes: number;
    you_liked: undefined | boolean;
    photo: string;
  
    constructor(
        publication_id: number, photo_id: number, publication_date: Date,
        username: string, specie: string, global_rating: number,
        num_likes: number, num_dislikes: number, you_liked: undefined | boolean,
        photo: string
    ) {
      this.publication_id = publication_id;
      this.photo_id = photo_id;
      this.publication_date = publication_date;
      this.username = username;
      this.specie = specie;
      this.global_rating = global_rating;
      this.num_likes = num_likes;
      this.num_dislikes = num_dislikes;
      this.you_liked = you_liked;
      this.photo = photo;
    }
  }