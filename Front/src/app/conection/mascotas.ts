export class Mascota {
  pet_id: number;
  name: string;
  description: string;
  species: string;
  photo_id: number;
  photo: string;

  constructor(
    pet_id: number = 0, name: string = "", description: string = "", 
    species: string = "", photo_id: number = 0, photo: string = ""
  ) {
    this.pet_id = pet_id;
    this.name = name;
    this.description = description;
    this.species = species;
    this.photo_id = photo_id;
    this.photo = photo;
  }
}