import { NONE_TYPE } from '@angular/compiler';
import { Component, OnInit, Inject, ViewChild, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { apiCall } from '../conection/apiRequest';
import { of, switchMap, throwError } from 'rxjs';

@Component({
  selector: 'app-registro2',
  templateUrl: './registro2.component.html',
  styleUrls: ['./registro2.component.css']
})
export class Registro2Component implements OnInit{

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiCall: apiCall,
  ) { }

  @ViewChild('fileButton') fileButton!: ElementRef;
  userID: string = '0';
  imagenSrc: string | ArrayBuffer | null = null; // Ruta de la imagen por defecto
  paramsRegistro2: any[] = [];
  realName: string = '';
  ubication: string = '';
  description: string = '';
  loginParams: any[] = [];

  onFileSelected(event: any): void {
    const fileInput = event.target as HTMLInputElement;
    const file = fileInput.files?.[0];

    if (!file) {
        alert('No se ha seleccionado ningún archivo.');
        return;
    }

    if (file.type !== 'image/png') {
        alert('Por favor, selecciona un archivo PNG.');
        fileInput.value = ''; // Limpiar la selección del archivo
        return;
    }

    const reader = new FileReader();
    reader.onload = (e: any) => {
        // Asigna la URL de datos a la propiedad imagenSrc para mostrar la vista previa
        this.imagenSrc = e.target.result;
    };
    reader.readAsDataURL(file);

    const fileName = file.name;
    if (fileName) {
        this.fileButton.nativeElement.innerText = `Has elegido: ${fileName}`;
    }
  }

  openFileInput(): void {
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (fileInput) {
      fileInput.click();
    }
  }

  ngOnInit(): void {
    // Obtener los parámetros de la página anterior
    this.paramsRegistro2 = history.state.paramsRegistro2;

    // Verificar si hay parámetros recibidos
    if (this.paramsRegistro2) {
      console.log("Parámetros recibidos:", this.paramsRegistro2);
      // Aquí puedes hacer lo que necesites con los parámetros recibidos
    } else {
      console.log("No se recibieron parámetros.");
    }
  }

  joinParams(): void {
    let id_imagen = 0;
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    const file = fileInput.files?.[0];

    // Verificar si hay un archivo seleccionado
    const subidaImagen = file ?
      this.apiCall.subirImagen(file) :
      of(0);

    subidaImagen.pipe(
      switchMap((response) => {
        id_imagen = response;
        const indiceEspacio = this.realName.indexOf(' ');

        // Separar el string en dos partes usando slice()
        const name = this.realName.slice(0, indiceEspacio);
        const surnames = this.realName.slice(indiceEspacio + 1);

        const body = {
          ...this.paramsRegistro2[0], // Si solo esperas un elemento en paramsRegistro2
          name: name,
          surnames: surnames,
          direction: this.ubication,
          description: this.description,
          photo_id: id_imagen
        };

        // Crear el usuario con la información combinada
        return this.apiCall.crearUsuario(body);
      })
    ).subscribe(() => {
      // Redirigir a la página de verificación una vez que se haya creado el usuario
      // Utiliza navigateByUrl en lugar de routerLink para poder pasar los parámetros en navigateByUrl
      this.router.navigateByUrl('/verificacion');
    }, (error) => {
      // Manejar el error si ocurriera
      console.error('Error:', error);
    });
  }
}
