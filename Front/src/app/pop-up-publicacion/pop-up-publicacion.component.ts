import { Component, OnInit, Inject, ViewChild, ElementRef} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { apiCall } from '../conection/apiRequest';
import { Mascota } from '../conection/mascotas';


@Component({
  selector: 'app-pop-up-publicacion',
  templateUrl: './pop-up-publicacion.component.html',
  styleUrls: ['./pop-up-publicacion.component.css']
})
export class PopUpPublicacionComponent implements OnInit {

  @ViewChild('fileButton') fileButton!: ElementRef;
  userID: string = '0';
  imagenSrc: string | ArrayBuffer | null = "../../assets/images/preview-foto.png"; // Ruta de la imagen por defecto
  error: number = 0;
  misMascotas: Mascota[] = [];
  mascotaElegida: Mascota = new Mascota;
  descripcion: string = '';
  public mostrarError: boolean = false;
  public error_sinmascotas: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PopUpPublicacionComponent>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiCall: apiCall,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userID = this.data.userID;
      this.apiCall.getMascotas(this.userID).subscribe(
        (mascotas: Mascota[]) => {
          this.misMascotas = mascotas;
          if (this.misMascotas.length == 0) {
            this.error_sinmascotas = true;
            // Si no hay mascotas, ajusta el tamaño del modal aquí
            this.dialogRef.updateSize('700px', '250px');
          }
        },
        (error: any) => {
          console.error('Error al obtener las mascotas:', error);
        }
      );
    })
  }

  onFileSelected(event: any): void {
    const fileInput = event.target as HTMLInputElement;
    const file = fileInput.files?.[0];

    if (!file) {
        alert('No se ha seleccionado ningún archivo.');
        return;
    }

    if (file.type !== 'image/png') {
        alert('Por favor, selecciona un archivo PNG.');
        fileInput.value = ''; // Limpiar la selección del archivo
        return;
    }

    const reader = new FileReader();
    reader.onload = (e: any) => {
        // Asigna la URL de datos a la propiedad imagenSrc para mostrar la vista previa
        this.imagenSrc = e.target.result;
    };
    reader.readAsDataURL(file);

    const fileName = file.name;
    if (fileName) {
        this.fileButton.nativeElement.innerText = `Has elegido: ${fileName}`;
    }
}

  openFileInput(): void {
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (fileInput) {
      fileInput.click();
    }
  }

  crearPublicacion(): void {
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    const file = fileInput.files?.[0];

    if (!file || !this.userID || !this.mascotaElegida.pet_id || !this.descripcion) {
        this.mostrarError = true;
        return;
    }

    this.apiCall.subirImagen(file).subscribe(
      (response) => {
        const body = {
            'user_id': parseInt(this.userID),
            'pet_id': this.mascotaElegida.pet_id,
            'photo_id': response,
            'description': this.descripcion
        };

        this.apiCall.crearPublicacion(body).subscribe(() => {
            // Call cerrarDialogo() after successfully creating the publication
            this.cerrarDialogo(true);
        });
      },
      (error) => {
          console.error('Error en la subida del archivo:', error);
      }
    );
  }

  cerrarDialogo(shouldReload: boolean) {
    this.dialogRef.close(shouldReload);
  }

}
