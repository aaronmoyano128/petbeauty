import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopUpPublicacionComponent } from './pop-up-publicacion.component';

describe('PopUpPublicacionComponent', () => {
  let component: PopUpPublicacionComponent;
  let fixture: ComponentFixture<PopUpPublicacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopUpPublicacionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PopUpPublicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
